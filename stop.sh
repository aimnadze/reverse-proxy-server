#!/bin/bash
cd `dirname $BASH_SOURCE`
if [ -f server.pid ]
then
    pid=`cat server.pid`
    kill $pid 2> /dev/null
    while ps -p $pid > /dev/null
    do
        sleep 0.2
    done
    ./clean.sh
fi
