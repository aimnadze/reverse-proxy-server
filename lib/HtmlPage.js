module.exports = (head, body) => {
    return (
        '<!DOCTYPE html>' +
        '<html>' +
            '<head>' +
                head +
                '<meta charset="UTF-8" />' +
                '<meta name="viewport" content="width=device-width" />' +
            '</head>' +
            '<body>' + body + '</body>' +
        '</html>'
    )
}
