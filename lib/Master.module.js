const cluster = require('cluster')
const os = require('os')

module.exports = app => () => {

    const listen = app.config.listen

    const log = app.Log()

    const addLogFile = app.LogFiles()

    const initMessage = app.InitMessage()

    app.TestServer(initMessage, listen.port, listen.host, () => {

        function fork () {
            const worker = cluster.fork()
            worker.on('disconnect', fork)
            worker.on('message', addLogFile)
        }

        for (let i = 0; i < os.cpus().length; i++) fork()

        app.Watch(['lib'])

        log.info('Started')
        log.info('Listening ' + (initMessage.httpsOptions.length === 0 ? 'http' : 'https') + '://' + listen.host + ':' + listen.port + '/')

    })

}
