const cluster = require('cluster')
const fs = require('fs')
const net = require('net')
const os = require('os')

const InitMessage = require('./InitMessage')
const LogFiles = require('./LogFiles')
const TestServer = require('./TestServer')

module.exports = () => {

    const logsDir = 'logs',
        logFiles = LogFiles(logsDir)

    let initMessage = InitMessage(logsDir)

    TestServer(initMessage.httpsOptions, initMessage.port, initMessage.host, () => {

        function fork () {

            const worker = cluster.fork()
            worker.on('online', () => {
                worker.on('disconnect', () => {
                    if (workers[id]) forkAlternative(id)
                })
                worker.send(initMessage)
            })
            worker.on('message', logFiles.add)

            const id = worker.id
            workers[id] = worker

        }

        function forkAlternative (id) {
            delete workers[id]
            fork()
        }

        function shutdown () {
            fs.unlinkSync('server.sock')
            process.exit(0)
        }

        const workers = Object.create(null)

        const numCpus = os.cpus().length
        for (let i = 0; i < numCpus; i++) fork()

        net.createServer(c => {
            c.end()
            delete require.cache[require.resolve('../config')]
            initMessage = InitMessage(logsDir)
            for (let i in workers) {
                workers[i].send('shutdown')
                forkAlternative(i)
            }
        }).listen('server.sock')

        process.on('SIGINT', shutdown)
        process.on('SIGTERM', shutdown)

    })

}
