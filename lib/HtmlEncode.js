module.exports = string => {
    string = String(string)
    string = string.replace(/&/g, '&amp;')
    string = string.replace(/"/g, '&quot;')
    string = string.replace(/</g, '&lt;')
    string = string.replace(/>/g, '&gt;')
    return string
}
