const http = require('http')
const https = require('https')

module.exports = initMessage => {

    const httpsOptions = initMessage.httpsOptions
    if (httpsOptions.length === 0) return http.createServer()

    return https.createServer({
        ...httpsOptions[0],
        SNICallback (host, callback) {
            const item = httpsOptions.find(item => (
                item.parsed_cert.checkHost(host) !== undefined
            ))
            callback(null, (item || httpsOptions[0]).context)
        },
    })

}
