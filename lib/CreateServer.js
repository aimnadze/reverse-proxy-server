const http = require('http')
const https = require('https')

module.exports = httpsOptions => {
    if (httpsOptions) return https.createServer(httpsOptions)
    return http.createServer()
}
