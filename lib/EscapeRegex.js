module.exports = s => {
    return s.replace(/([.*+?=^!:${}()|[\]\/\\])/g, '\\$1')
}
