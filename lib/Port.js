const config = require('../config')

module.exports = httpsOptions => {
    const port = config.port
    if (port) return port
    return httpsOptions ? 443 : 80
}
