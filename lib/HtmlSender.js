const headers = { 'content-type': 'text/html; charset=UTF-8' }

module.exports = (statusCode, html, logFile) => (res, logLine) => {

    logLine.setResponse(statusCode, headers)
    logFile.add(logLine)

    res.writeHead(statusCode, headers)
    res.end(html)

}
