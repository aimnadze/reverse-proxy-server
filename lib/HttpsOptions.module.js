const crypto = require('crypto')
const fs = require('fs')
const tls = require('tls')

module.exports = app => () => (
    (app.config.https || []).map(item => {

        const cert = fs.readFileSync(item.cert, 'utf8')

        const context_options = {
            key: fs.readFileSync(item.key, 'utf8'),
            cert,
        }

        return {
            ...context_options,
            parsed_cert: new crypto.X509Certificate(cert),
            context: tls.createSecureContext(context_options),
        }

    })
)
