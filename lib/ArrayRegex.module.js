module.exports = app => array => {
    const parts = []
    app.ForIn(array, part => {
        parts.push(part.replace(/([.*+?=^!:${}()|[\]/\\])/g, '\\$1'))
    })
    return parts.join('|')
}
