module.exports = (callback, milliseconds) => {
    const timeout = setTimeout(callback, milliseconds)
    return () => {
        clearTimeout(timeout)
    }
}
