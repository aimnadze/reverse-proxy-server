module.exports = () => {

    const map = Object.create(null)
    const array = []

    return {
        get (name) {
            const value = map[name]
            if (value !== undefined) return value
            const item = array.find(item => item.pattern.test(name))
            return item === undefined ? undefined : item.value
        },
        put (name, value) {

            const match = name.match(/^\/(.*)\/(.*?)$/)
            if (match === null) {
                map[name] = value
                return
            }

            array.push({
                value,
                pattern: new RegExp(match[1], match[2]),
            })

        },
    }

}
