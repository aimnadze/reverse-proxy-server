const fs = require('fs')

const ForEach = require('./ForEach')

module.exports = rawPages => {
    const pages = Object.create(null)
    ForEach(rawPages, (filename, statusCode) => {
        try {
            pages[statusCode] = fs.readFileSync(filename, 'utf8')
        } catch (e) {
            console.error('WARNING: failed to read error page "' + filename + '"')
        }
    })
    return pages
}
