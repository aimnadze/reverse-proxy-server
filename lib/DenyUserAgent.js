module.exports = (denyUserAgents, forbidden) => {

    if (denyUserAgents === '') return () => false

    const regex = new RegExp(denyUserAgents)

    return (req, res, logLine) => {

        const userAgent = req.headers['user-agent']
        if (userAgent === undefined) return false

        if (!regex.test(userAgent)) return false

        forbidden(res, logLine)
        return true

    }

}
