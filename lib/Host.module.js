const url = require('url')

module.exports = app => (raw_host, timeout, retry) => {

    const host = (() => {

        if (typeof raw_host !== 'string') return raw_host

        const parsed_url = url.parse(raw_host)

        return {
            protocol: parsed_url.protocol.slice(0, -1),
            host: parsed_url.hostname,
            port: Number(parsed_url.port),
        }

    })()

    const protocol = host.protocol === 'https' ? 'https' : 'http'

    return {
        protocol,
        host: host.host,
        port: host.port || (protocol === 'https' ? 443 : 80),
        timeout: host.timeout || timeout,
        retry: host.retry === undefined ? retry : host.retry,
        denyUserAgents: app.ArrayRegex(host.denyUserAgents),
    }

}
