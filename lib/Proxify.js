const FixHeaders = require('./FixHeaders')

module.exports = (removeHeaders, replaceErrorPages, logFile) => {
    return (req, res, host, logLine) => {

        function sendBadGateway () {
            host.sendBadGateway(res, logLine)
        }

        const reqHeaders = req.headers

        const denyUserAgents = host.denyUserAgents
        if (denyUserAgents !== undefined) {
            const userAgent = reqHeaders['user-agent']
            if (userAgent !== undefined && denyUserAgents.test(userAgent)) {
                host.sendForbidden(res, logLine)
                return
            }
        }

        if (req.httpVersion === '1.0') reqHeaders.connection = 'close'
        else reqHeaders.connection = 'keep-alive'

        let xForwardedFor = reqHeaders['x-forwarded-for']
        if (xForwardedFor) xForwardedFor += ', '
        else xForwardedFor = ''
        xForwardedFor += req.connection.remoteAddress
        reqHeaders['x-forwarded-for'] = xForwardedFor

        FixHeaders(reqHeaders)

        const options = {
            host: host.host,
            port: host.port,
            method: req.method,
            path: req.url,
            headers: reqHeaders,
        }

        let proxyConnectTime

        const proxyReqTime = Date.now(),
            time = {}

        logLine.logObject.time = time

        const proxyReq = host.request(options, proxyRes => {

            time.proxyWait = Date.now() - proxyConnectTime

            clearTimeout(abortTimeout)

            proxyReq.removeListener('error', sendBadGateway)
            proxyReq.on('error', () => {
                res.end()
            })
            proxyReq.setTimeout(timeout, () => {
                proxyReq.abort()
            })

            for (let i in removeHeaders) {
                delete proxyRes.headers[removeHeaders[i]]
            }

            const statusCode = proxyRes.statusCode
            const hostReplaceErrorPage = host.replaceErrorPages[statusCode]
            if (hostReplaceErrorPage) {
                proxyReq.abort()
                hostReplaceErrorPage(res, logLine)
            } else {
                const replaceErrorPage = replaceErrorPages[statusCode]
                if (replaceErrorPage) {
                    proxyReq.abort()
                    replaceErrorPage(res, logLine)
                } else {
                    const headers = proxyRes.headers
                    logLine.setResponse(statusCode, headers)
                    logFile.add(logLine)
                    res.writeHead(statusCode, headers)
                    proxyRes.pipe(res)
                }
            }

        })
        proxyReq.on('socket', () => {
            proxyReq.socket.on('connect', () => {
                proxyConnectTime = Date.now()
                time.proxyConnect = proxyConnectTime - proxyReqTime
            })
        })

        const timeout = host.timeout

        const abortTimeout = setTimeout(() => {
            proxyReq.removeListener('error', sendBadGateway)
            proxyReq.on('error', () => {
                host.sendGatewayTimeout(res, logLine)
            })
            proxyReq.abort()
        }, timeout)

        proxyReq.on('error', sendBadGateway)
        req.pipe(proxyReq)
        res.on('close', () => {
            req.unpipe(proxyReq)
            proxyReq.abort()
        })
        res.on('error', () => {
            proxyReq.removeListener('error', sendBadGateway)
            proxyReq.abort()
        })

    }
}
