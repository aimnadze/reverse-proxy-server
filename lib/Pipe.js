module.exports = (from, to) => {

    function from_data (data) {
        bytes += data.length
        if (to.write(data) === false) from.pause()
    }

    function from_end () {
        to.end()
    }

    function to_drain () {
        from.resume()
    }

    let bytes = 0
    from.on('data', from_data)
    from.on('end', from_end)
    to.on('drain', to_drain)

    return {
        bytes () {
            return bytes
        },
        unpipe () {
            from.off('data', from_data)
            from.off('end', from_end)
            to.off('drain', to_drain)
        },
    }

}
