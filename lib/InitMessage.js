const ArrayRegex = require('./ArrayRegex')
const ErrorPages = require('./ErrorPages')
const ForEach = require('./ForEach')
const HttpsOptions = require('./HttpsOptions')
const Port = require('./Port')

const config = require('../config')

module.exports = logsDir => {

    const httpsOptions = HttpsOptions()
    const port = Port(httpsOptions)

    const timeout = config.timeout || 60000

    const hosts = Object.create(null)
    ForEach(config.hosts, (host, hostname) => {
        hosts[hostname] = {
            host: host.host,
            port: host.port,
            protocol: host.protocol === 'https' ? 'https' : 'http',
            timeout: host.timeout || timeout,
            errorPages: ErrorPages(host.errorPages),
            replaceErrorPages: ErrorPages(host.replaceErrorPages),
            denyUserAgents: ArrayRegex(host.denyUserAgents),
        }
    })

    const redirectHosts = Object.create(null)
    ForEach(config.redirectHosts, (target, host) => {
        if (typeof target === 'string') {
            target = { code: 301, host: target }
        }
        redirectHosts[host] = target
    })

    const directories = Object.create(null)
    ForEach(config.directories, (path, directory) => {
        directories[directory] = String(path)
    })

    const addHeaders = Object.create(null)
    ForEach(config.addHeaders, (value, header) => {
        if (value instanceof Array) value = value.map(String)
        else value = String(value)
        addHeaders[header] = value
    })

    const removeHeaders = []
    ForEach(config.removeHeaders, header => {
        header = header.toLowerCase()
        if (removeHeaders.indexOf(header) !== -1) return
        removeHeaders.push(header)
    })

    return {
        host: config.host,
        port: port,
        timeout: timeout,
        hosts: hosts,
        redirectHosts: redirectHosts,
        directories: directories,
        addHeaders: addHeaders,
        removeHeaders: removeHeaders,
        errorPages: ErrorPages(config.errorPages),
        replaceErrorPages: ErrorPages(config.replaceErrorPages),
        logsDir: logsDir,
        httpsOptions: httpsOptions,
        denyUserAgents: ArrayRegex(config.denyUserAgents),
    }

}
