module.exports = headers => {
    Object.keys(headers).forEach(key => {
        const fixedKey = key.replace(/[^a-zA-Z0-9_!#$%&'*+.^`|~-]/g, '')
        if (fixedKey === key) return
        const value = headers[key]
        delete headers[key]
        if (fixedKey !== '') headers[fixedKey] = value
    })
}
