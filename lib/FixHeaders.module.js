const http = require('http')

module.exports = app => (initMessage, logLine, req) => {

    const headers = app.ObjectCopy(req.headers)

    if (req.httpVersion === '1.0') headers.connection = 'close'
    else headers.connection = 'keep-alive'

    if (initMessage.httpsOptions.length !== 0) {
        headers['x-forwarded-proto'] = 'https'
    }

    let xForwardedFor = headers['x-forwarded-for']
    if (xForwardedFor) xForwardedFor += ', '
    else xForwardedFor = ''
    xForwardedFor += logLine.logObject.request.remoteAddress
    headers['x-forwarded-for'] = xForwardedFor

    for (const i in headers) {
        try {
            http.validateHeaderName(i)
            http.validateHeaderValue(i, headers[i])
        } catch (e) {
            if (!(e instanceof TypeError)) throw e
            delete headers[i]
        }
    }

    return headers

}
