const fs = require('fs')

module.exports = app => onRelease => {

    function release () {
        const fileToRelease = file
        stream.end(() => {
            onRelease(fileToRelease)
        })
    }

    function releaseAndReset () {
        release()
        reset()
    }

    function reset () {
        file = 'access-log/' + app.RandomBytes(8).toString('hex')
        stream = fs.createWriteStream(file)
        numLines = 0
        abort_timeout = app.Timeout(releaseAndReset, 1000 * 60 * 60 * 24)
    }

    function shutdown () {
        abort_timeout()
        release()
    }

    let file, stream, numLines, abort_timeout
    let shuttingDown = false
    let numPendingLines = 0

    reset()

    return {
        add (logLine) {
            stream.write(logLine.toJsonLine())
            numLines++
            numPendingLines--
            if (shuttingDown && !numPendingLines) {
                shutdown()
            } else if (numLines === 1024 * 8) {
                abort_timeout()
                releaseAndReset()
            }
        },
        newLine (req) {
            numPendingLines++
            return app.LogLine(req)
        },
        shutdown () {
            shuttingDown = true
            if (!numPendingLines) shutdown()
        },
    }

}
