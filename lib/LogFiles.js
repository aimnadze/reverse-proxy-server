const fs = require('fs')
const path = require('path')

module.exports = () => {

    const doneExtension = '.done'

    const maxNumber = 64

    const files = fs.readdirSync('access-log').filter(name => (
        name !== '.gitignore'
    )).map(name => {
        let file = 'access-log/' + name
        if (path.extname(name) !== doneExtension) {
            const newFile = file + doneExtension
            fs.renameSync(file, newFile)
            file = newFile
        }
        return { file, stat: fs.statSync(file) }
    }).sort((a, b) => (
        a.stat.mtime > b.stat.mtime ? 1 : -1
    )).map(item => item.file)

    while (files.length > maxNumber) fs.unlinkSync(files.shift())

    return file => {
        const newFile = file + doneExtension
        fs.rename(file, newFile, () => {
            files.push(newFile)
            if (files.length > maxNumber) {
                fs.unlink(files.shift(), () => {})
            }
        })
    }

}
