const fs = require('fs')
const path = require('path')

module.exports = logsDir => {

    try {
        const stat = fs.statSync(logsDir)
        if (!stat.isDirectory()) {
            console.error('ERROR: log directory ' + JSON.stringify(logsDir) + ' is not a directory')
            process.exit(1)
        }
    } catch (e) {
        if (e.code !== 'ENOENT') throw e
        fs.mkdirSync(logsDir)
    }

    const doneExtension = '.done'

    const maxNumber = 64

    const files = fs.readdirSync(logsDir).map(name => {
        let file = logsDir + '/' + name
        if (path.extname(name) !== doneExtension) {
            const newFile = file + doneExtension
            fs.renameSync(file, newFile)
            file = newFile
        }
        return {
            file: file,
            stat: fs.statSync(file),
        }
    }).sort((a, b) => {
        return a.stat.mtime > b.stat.mtime ? 1 : -1
    }).map(item => {
        return item.file
    })

    while (files.length > maxNumber) fs.unlinkSync(files.shift())

    return {
        add: file => {
            const newFile = file + doneExtension
            fs.rename(file, newFile, () => {
                files.push(newFile)
                if (files.length > maxNumber) {
                    fs.unlink(files.shift(), () => {})
                }
            })
        },
    }

}
