module.exports = app => () => {

    ;(() => {

        const config = app.config
        if (config.listen === undefined) config.listen = {}

        const listen = config.listen
        if (listen.port === undefined) {
            listen.port = config.https === undefined ? 80 : 443
        }

        if (listen.host === undefined) listen.host = ''

    })()

    if (require('cluster').isMaster) app.Master()
    else app.Worker()

}
