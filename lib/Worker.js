const fs = require('fs')
const http = require('http')
const https = require('https')

const CreateServer = require('./CreateServer')
const ForEach = require('./ForEach')
const HtmlPage = require('./HtmlPage')
const HtmlSender = require('./HtmlSender')
const LogFile = require('./LogFile')
const Proxify = require('./Proxify')
const RedirectSender = require('./RedirectSender')

module.exports = () => {
    process.once('message', initMessage => {

        function parseDenyUserAgents (denyUserAgents) {
            if (denyUserAgents !== '') return new RegExp(denyUserAgents)
        }

        const logFile = LogFile(initMessage.logsDir, logFile => {
            process.send(logFile)
        })

        const addHeaders = initMessage.addHeaders,
            directories = initMessage.directories,
            removeHeaders = initMessage.removeHeaders

        const errorPages = Object.create(null)
        ForEach([403, 404, 500, 502, 504], statusCode => {

            const html = initMessage.errorPages[statusCode]
            if (html) {
                errorPages[statusCode] = HtmlSender(statusCode, html, logFile)
                return
            }

            ;(() => {
                const title = statusCode + ' ' + http.STATUS_CODES[statusCode]
                const html = HtmlPage(
                    '<title>' + title + '</title>',
                    '<h1>' + title + '</h1>'
                )
                errorPages[statusCode] = HtmlSender(statusCode, html, logFile)
            })()

        })

        const errorPage403 = errorPages[403],
            errorPage404 = errorPages[404],
            errorPage500 = errorPages[500]

        const replaceErrorPages = Object.create(null)
        ForEach(initMessage.replaceErrorPages, (html, statusCode) => {
            replaceErrorPages[statusCode] = HtmlSender(statusCode, html, logFile)
        })

        const plainHosts = Object.create(null),
            regexHosts = []
        ForEach(initMessage.hosts, (host, i) => {

            function getErrorPageSender (code) {
                const html = host.errorPages[code]
                if (html) return HtmlSender(code, html, logFile)
                return errorPages[code]
            }

            ForEach(host.replaceErrorPages, (html, statusCode) => {
                host.replaceErrorPages[statusCode] = HtmlSender(statusCode, html, logFile)
            })

            host.sendForbidden = getErrorPageSender(403)
            host.sendBadGateway = getErrorPageSender(502)
            host.sendGatewayTimeout = getErrorPageSender(504)
            host.denyUserAgents = parseDenyUserAgents(host.denyUserAgents)

            if (host.protocol === 'https') {
                host.request = (options, callback) => {
                    options.rejectUnauthorized = false
                    return https.request(options, callback)
                }
            } else {
                host.request = (options, callback) => {
                    return http.request(options, callback)
                }
            }

            const match = i.match(/^\/(.*)\/(.*?)$/)
            if (match) {
                regexHosts.push({
                    regex: new RegExp(match[1], match[2]),
                    forward: host,
                })
            } else {
                plainHosts[i] = host
            }
        })

        const redirectHosts = Object.create(null)
        ForEach(initMessage.redirectHosts, (redirectHost, hostname) => {
            redirectHosts[hostname] = RedirectSender(redirectHost, logFile)
        })

        const denyUserAgents = parseDenyUserAgents(initMessage.denyUserAgents)

        const proxify = Proxify(removeHeaders, replaceErrorPages, logFile)

        http.globalAgent.maxSockets = 1024

        const server = CreateServer(initMessage.httpsOptions)
        server.listen(initMessage.port, initMessage.host)
        server.on('request', (req, res) => {

            for (let i in addHeaders) res.setHeader(i, addHeaders[i])

            const reqHeaders = req.headers
            const logLine = logFile.newLine(req)

            if (denyUserAgents !== undefined) {
                const userAgent = reqHeaders['user-agent']
                if (userAgent !== undefined && denyUserAgents.test(userAgent)) {
                    errorPage403(res, logLine)
                    return
                }
            }

            const url = req.url
            for (let i in directories) {

                if (!url.startsWith(i)) continue

                const stream = fs.createReadStream(directories[i] + url.substr(i.length))
                stream.pipe(res)
                stream.on('error', err => {

                    const code = err.code
                    if (code === 'ENOENT' || code === 'EISDIR') {
                        errorPage404(res, logLine)
                        return
                    }

                    errorPage500(res, logLine)

                })
                return

            }

            const hostHeader = reqHeaders.host,
                redirectHost = redirectHosts[hostHeader]
            if (redirectHost) {
                redirectHost(req, res, logLine)
            } else {
                const plainHost = plainHosts[hostHeader]
                if (plainHost) {
                    proxify(req, res, plainHost, logLine)
                } else {
                    for (let i in regexHosts) {
                        const regexHost = regexHosts[i]
                        if (regexHost.regex.test(hostHeader)) {
                            proxify(req, res, regexHost.forward, logLine)
                            return
                        }
                    }
                    errorPage404(res, logLine)
                }
            }

        })

        process.once('message', () => {
            server.close()
            logFile.shutdown()
        })

    })
}
