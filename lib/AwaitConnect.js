module.exports = (req, done) => {

    const socket = req.socket
    if (socket.connecting) {
        socket.once('connect', done)
        return
    }

    done()

}
