const crypto = require('crypto')
const fs = require('fs')

const LogLine = require('./LogLine')

module.exports = (logsDir, onRelease) => {

    function release () {
        const fileToRelease = file
        stream.end(() => {
            onRelease(fileToRelease)
        })
    }

    function releaseAndReset () {
        release()
        reset()
    }

    function reset () {
        file = logsDir + '/' + crypto.randomBytes(8).toString('hex')
        stream = fs.createWriteStream(file)
        numLines = 0
        timeout = setTimeout(releaseAndReset, 1000 * 60 * 60 * 24)
    }

    function shutdown () {
        clearTimeout(timeout)
        release()
    }

    let file, stream, numLines, timeout
    let shuttingDown = false
    let numPendingLines = 0

    reset()

    return {
        add: logLine => {
            stream.write(logLine.toJsonLine())
            numLines++
            numPendingLines--
            if (shuttingDown && !numPendingLines) {
                shutdown()
            } else if (numLines === 1024 * 8) {
                clearTimeout(timeout)
                releaseAndReset()
            }
        },
        newLine: req => {
            numPendingLines++
            return LogLine(req)
        },
        shutdown: () => {
            shuttingDown = true
            if (!numPendingLines) shutdown()
        },
    }

}
