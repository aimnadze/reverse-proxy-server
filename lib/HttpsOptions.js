const fs = require('fs')

const config = require('../config')

module.exports = () => {
    const httpsOptions = config.https
    if (httpsOptions) {

        httpsOptions.key = fs.readFileSync(httpsOptions.key, 'utf8')
        httpsOptions.cert = fs.readFileSync(httpsOptions.cert, 'utf8')

        const ca = httpsOptions.ca
        for (let i in ca) ca[i] = fs.readFileSync(ca[i], 'utf8')
        httpsOptions.ca = ca

    }
    return httpsOptions
}
