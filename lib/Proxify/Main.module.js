module.exports = app => (initMessage, logFile, host) => {

    const request = app.Proxify.Request(initMessage, logFile, host)

    return (req, res, logLine) => {

        if (host.denyUserAgent(req, res, logLine)) return

        logLine.logObject.proxy = {
            time: { connect: 0, wait: 0 },
        }

        const request_time = Date.now()

        ;(() => {

            function next () {
                request(req, res, logLine, () => {

                    if (index === host.retry) {
                        host.sendBadGateway(res, logLine)
                        return
                    }

                    index++
                    app.Timeout(next, 2000)

                }, {
                    request_time,
                })
            }

            let index = 0
            next()

        })()

    }

}
