module.exports = app => (initMessage, logFile, host) => (req, res, logLine, sendBadGateway, options) => {

    const proxyReq = host.request({
        host: host.host,
        port: host.port,
        method: req.method,
        path: req.url,
        headers: app.FixHeaders(initMessage, logLine, req),
    })
    proxyReq.on('error', sendBadGateway)
    proxyReq.on('socket', () => {
        app.AwaitConnect(proxyReq, () => {

            const connect_time = Date.now()
            logLine.logObject.proxy.time.connect = connect_time - options.request_time
            proxyReq.on('response', proxyRes => {

                logLine.logObject.proxy.time.wait = Date.now() - connect_time

                abort_timeout()

                proxyReq.removeListener('error', sendBadGateway)
                proxyReq.on('error', () => {
                    res.end()
                })
                proxyReq.setTimeout(timeout, () => {
                    proxyReq.abort()
                })
                proxyReq.on('close', () => {
                    logLine.logObject.proxy.bytes = {
                        received: req_pipe.bytes(),
                        sent: res_pipe.bytes(),
                    }
                    logLine.setResponse(statusCode, headers)
                    logFile.add(logLine)
                })

                app.ForIn(initMessage.removeHeaders, name => {
                    delete proxyRes.headers[name]
                })

                const statusCode = proxyRes.statusCode
                const headers = proxyRes.headers
                res.writeHead(statusCode, headers)
                const res_pipe = app.Pipe(proxyRes, res)

            })

            const req_pipe = app.Pipe(req, proxyReq)
            res.on('close', req_pipe.unpipe)

        })
    })

    const timeout = host.timeout

    const abort_timeout = app.Timeout(() => {
        proxyReq.removeListener('error', sendBadGateway)
        proxyReq.on('error', () => {
            host.sendGatewayTimeout(res, logLine)
        })
        proxyReq.abort()
    }, timeout)

    res.on('close', () => {
        proxyReq.abort()
    })
    res.on('error', () => {
        proxyReq.removeListener('error', sendBadGateway)
        proxyReq.abort()
    })

}
