const fs = require('fs')
const http = require('http')
const https = require('https')

module.exports = app => () => {

    const initMessage = app.InitMessage()

    const listen = app.config.listen

    const logFile = app.LogFile(logFile => {
        process.send(logFile)
    })

    const directories = initMessage.directories
    const errorPages = app.ErrorPages(logFile)

    const hostMap = app.HostMap()
    app.ForIn(initMessage.redirectHosts, (host, i) => {
        hostMap.put(i, app.RedirectSender(host, logFile))
    })
    app.ForIn(initMessage.hosts, (host, i) => {

        host.sendForbidden = errorPages[403]
        host.sendBadGateway = errorPages[502]
        host.sendGatewayTimeout = errorPages[504]
        host.denyUserAgent = app.DenyUserAgent(host.denyUserAgents, host.sendForbidden)

        if (host.protocol === 'https') {
            host.request = (options, callback) => {
                options.rejectUnauthorized = false
                return https.request(options, callback)
            }
        } else {
            host.request = (options, callback) => (
                http.request(options, callback)
            )
        }

        hostMap.put(i, app.Proxify.Main(initMessage, logFile, host))

    })

    const denyUserAgent = app.DenyUserAgent(initMessage.denyUserAgents, errorPages[403])

    http.globalAgent.maxSockets = 1024

    const rateLimit = app.RateLimit(errorPages)

    const server = app.CreateServer(initMessage)
    server.listen(listen.port, listen.host)
    server.on('request', (req, res) => {

        const reqHeaders = req.headers
        const logLine = logFile.newLine(req)
        if (rateLimit(req, res, logLine)) return

        app.ForIn(initMessage.addHeaders, (value, i) => {
            res.setHeader(i, value)
        })

        if (denyUserAgent(req, res, logLine)) return

        const url = req.url
        for (const i in directories) {

            if (!url.startsWith(i)) continue

            const stream = fs.createReadStream(directories[i] + url.substr(i.length))
            stream.pipe(res)
            stream.on('error', err => {

                const code = err.code
                if (code === 'ENOENT' || code === 'EISDIR') {
                    errorPages[404](res, logLine)
                    return
                }

                errorPages[500](res, logLine)

            })
            return

        }

        const hostHeader = reqHeaders.host

        const host = hostMap.get(hostHeader)
        if (host) {
            host(req, res, logLine)
            return
        }

        errorPages[404](res, logLine)

    })

}
