const http = require('http')

module.exports = app => logFile => {
    const pages = Object.create(null)
    app.ForIn([403, 404, 429, 500, 502, 504], statusCode => {
        const title = statusCode + ' ' + http.STATUS_CODES[statusCode]
        const html = app.HtmlPage(
            '<title>' + title + '</title>',
            '<h1>' + title + '</h1>'
        )
        pages[statusCode] = app.HtmlSender(statusCode, html, logFile)
    })
    return pages
}
