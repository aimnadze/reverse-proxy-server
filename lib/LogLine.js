module.exports = req => {

    const logObject = {
        request: {
            remoteAddress: (() => {
                const address = String(req.socket.remoteAddress)
                const match = address.match(/^::ffff:(\d+\.\d+\.\d+\.\d+)$/)
                return match === null ? address : match[1]
            })(),
            method: req.method,
            url: req.url,
            time: (new Date).toISOString(),
            headers: req.headers,
        },
    }

    return {
        logObject,
        setResponse (statusCode, headers) {
            logObject.response = { statusCode, headers }
        },
        toJsonLine () {
            return JSON.stringify(logObject) + '\n'
        },
    }

}
