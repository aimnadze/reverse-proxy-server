module.exports = req => {

    const logObject = {
        request: {
            remoteAddress: req.connection.remoteAddress,
            method: req.method,
            url: req.url,
            time: (new Date).toISOString(),
            headers: req.headers,
        },
    }

    return {
        logObject: logObject,
        setResponse: (statusCode, headers) => {
            logObject.response = {
                statusCode: statusCode,
                headers: headers,
            }
        },
        toJsonLine: () => {
            return JSON.stringify(logObject) + '\n'
        },
    }

}
