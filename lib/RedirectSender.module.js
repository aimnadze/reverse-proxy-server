const http = require('http')

module.exports = app => (redirectHost, logFile) => {

    const statusCode = redirectHost.code
    const title = statusCode + ' ' + http.STATUS_CODES[statusCode]

    return (req, res, logLine) => {

        const location = redirectHost.host + req.url
        const headers = {
            'content-type': 'text/html; charset=UTF-8',
            'location': location,
        }

        logLine.setResponse(statusCode, headers)
        logFile.add(logLine)

        res.writeHead(statusCode, headers)
        res.end(
            app.HtmlPage(
                '<title>' + title + '</title>',
                '<h1>' + title + '</h1>' +
                '<p>' +
                    'The document has moved ' +
                    '<a href="' + app.HtmlEncode(location) + '">here</a>.' +
                '</p>'
            )
        )

    }

}
