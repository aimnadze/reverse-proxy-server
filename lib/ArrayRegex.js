const EscapeRegex = require('./EscapeRegex')

module.exports = array => {
    const parts = []
    for (let i in array) parts.push(EscapeRegex(array[i]))
    return parts.join('|')
}
