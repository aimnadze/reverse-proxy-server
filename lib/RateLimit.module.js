module.exports = app => errorPages => {

    const rateLimits = app.config.rateLimitUserAgents || {}
    const checks = Object.keys(rateLimits).map(name => {

        let number = 0
        const rateLimit = rateLimits[name]

        return (userAgent, res, logLine) => {

            if (userAgent.indexOf(name) === -1) return true

            if (number === rateLimit.number) {
                errorPages[429](res, logLine)
                return false
            }

            number++
            app.Timeout(() => {
                number--
            }, rateLimit.seconds * 1000)

            return true

        }

    })

    return (req, res, logLine) => {

        const userAgent = req.headers['user-agent']
        if (userAgent === undefined) return false

        return !checks.every(check => (
            check(userAgent, res, logLine)
        ))

    }

}
