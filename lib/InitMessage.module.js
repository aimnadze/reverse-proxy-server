module.exports = app => () => {

    const timeout = app.config.timeout || 60000
    const retry = app.config.retry || 0

    const hosts = Object.create(null)
    app.ForIn(app.config.hosts, (host, hostname) => {
        hosts[hostname] = app.Host(host, timeout, retry)
    })

    const redirectHosts = Object.create(null)
    app.ForIn(app.config.redirectHosts, (target, host) => {
        if (typeof target === 'string') {
            target = { code: 301, host: target }
        }
        redirectHosts[host] = target
    })

    const directories = Object.create(null)
    app.ForIn(app.config.directories, (path, directory) => {
        directories[directory] = String(path)
    })

    const addHeaders = Object.create(null)
    app.ForIn(app.config.addHeaders, (value, header) => {
        if (value instanceof Array) value = value.map(String)
        else value = String(value)
        addHeaders[header] = value
    })

    const removeHeaders = []
    app.ForIn(app.config.removeHeaders, header => {
        header = header.toLowerCase()
        if (removeHeaders.indexOf(header) !== -1) return
        removeHeaders.push(header)
    })

    return {
        timeout, retry, hosts, redirectHosts, directories,
        addHeaders, removeHeaders,
        denyUserAgents: app.ArrayRegex(app.config.denyUserAgents),
        httpsOptions: app.HttpsOptions(),
    }

}
