module.exports = {

    debug_mode: true,

    listen: {

        // Port to listen to
        port: 80,

        // Host to listen to
        //
        // Examples:
        // host: '',            // bind to all interfaces
        // host: '0.0.0.0',     // bind to all ipv4 interfaces
        // host: '::',          // bind to all ipv6 interfaces
        // host: '<ipAddress>', // bind to a specific ip address
        host: '',

    },

    // Proxy request timeout
    timeout: 60000,

    // Number of times to retry
    retry: 1,

    // Hosts to proxy requests for
    //
    // Example:
    // hosts: {
    //     'example.org': 'http://127.0.0.1:8080',
    //     'example.com': {
    //         host: '127.0.0.1',
    //         port: 8080,
    //     },
    //     'subdomain.example.com': {
    //         host: '127.0.0.1',
    //         port: 8080,
    //         // override global timeout
    //         timeout: 10000,
    //         // override global retry
    //         retry: 2,
    //     },
    //     'secure.example.com': {
    //         protocol: 'https',
    //         host: '127.0.0.1',
    //         port: 443,
    //     },
    //     '/^regex\\.example\\.com$/': {
    //         host: '127.0.0.1',
    //         port: 8080,
    //     },
    //     'another.example.com': {
    //         host: '127.0.0.1',
    //         port: 8080,
    //         // forbid access to certain user agents per host
    //         denyUserAgents: [
    //             'SpammerBot',
    //             'AnotherBot',
    //             // ...
    //         ],
    //     },
    //     // ...
    // },
    hosts: {
    },

    // Redirect certain hosts to other locations
    //
    // Example:
    // redirectHosts: {
    //    'www.example.com': 'http://example.com',
    //    'my.example.com': 'http://example.com/my',
    //    'your.example.com': {
    //        code: 301, // moved permanently
    //        host: 'http://example.com/your',
    //    },
    //    'his.example.com': {
    //        code: 302, // found
    //        host: 'http://example.com/his',
    //    },
    //    'her.example.com': {
    //        code: 307, // temporary redirect
    //        host: 'http://example.com/her',
    //    },
    //    // ...
    // },
    // Note 1: No trailing slashes are required
    // Note 2: Default redirect code is 301 (moved permanently)
    redirectHosts: {
    },

    // Add custom headers to all responses
    //
    // Example:
    // addHeaders: {
    //     'server': 'whatever server',
    //     'strict-transport-security': 'max-age=' + (60 * 60 * 24 * 30),
    //     'cookie': ['a=b', 'c=d'],
    // },
    addHeaders: {
    },

    // Serve static files from certain directories
    //
    // Example:
    // directories: {
    //     '/public/': 'public/'
    // },
    directories: {
    },

    // Remove headers from all proxy responses
    //
    // Example:
    // removeHeaders: [
    //     'pragma',
    //     'server',
    //     'x-powered-by',
    //     // ...
    // ],
    // Note: Headers should be all lowercase
    removeHeaders: [
    ],

    // Listen to HTTPS instead of HTTP
    //
    // Example:
    // https: [
    //     {
    //         key: 'ssl/key',
    //         cert: 'ssl/cert',
    //     },
    //     // ...
    // ],

    // Forbid access to certain user agents
    //
    // Example:
    // denyUserAgents: [
    //     'SpammerBot',
    //     'AnotherBot',
    //     // ...
    // ],
    denyUserAgents: [
    ],

    // Rate limit certain user agents
    //
    // Example:
    // rateLimitUserAgents: {
    //     'SpammerBot': {
    //         number: 60, // number of requests
    //         seconds: 60,
    //     },
    //     // ...
    // ],
    rateLimits: {
    },

}
