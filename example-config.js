// Port to listen to
exports.port = 80

// Host to listen to
//
// Examples:
// exports.host = ''            // bind to all interfaces
// exports.host = '0.0.0.0'     // bind to all ipv4 interfaces
// exports.host = '::'          // bind to all ipv6 interfaces
// exports.host = '<ipAddress>' // bind to a specific ip address
exports.host = ''

// Proxy request timeout
exports.timeout = 60000

// Hosts to proxy requests for
//
// Example:
// exports.hosts = {
//     'example.com': {
//         host: '127.0.0.1',
//         port: 8080,
//     },
//     'subdomain.example.com': {
//         host: '127.0.0.1',
//         port: 8080,
//         // override global timeout
//         timeout: 10000,
//     },
//     'secure.example.com': {
//         host: '127.0.0.1',
//         port: 443,
//         protocol: 'https',
//     },
//     '/^regex\\.example\\.com$/': {
//         host: '127.0.0.1',
//         port: 8080,
//     },
//     'another.example.com': {
//         host: '127.0.0.1',
//         port: 8080,
//         // override global errorPages
//         errorPages: {
//             502: 'error-pages/another.example.com/502-bad-gateway.html',
//             504: 'error-pages/another.example.com/504-gateway-timeout.html',
//         },
//         // override global replaceErrorPages
//         replaceErrorPages: {
//             403: 'replace-error-pages/another.example.com/403-forbidden.html',
//             404: 'replace-error-pages/another.example.com/404-not-found.html',
//             // ...
//         },
//         // forbid access to certain user agents per host
//         exports.denyUserAgents = [
//             'SpammerBot',
//             'AnotherBot',
//             ...
//         ],
//     },
//     // ...
// }
exports.hosts = {
}

// Redirect certain hosts to other locations
//
// Example:
// exports.redirectHosts = {
//    'www.example.com': 'http://example.com',
//    'my.example.com': 'http://example.com/my',
//    'your.example.com': {
//        code: 301, // moved permanently
//        host: 'http://example.com/your',
//    },
//    'his.example.com': {
//        code: 302, // found
//        host: 'http://example.com/his',
//    },
//    'her.example.com': {
//        code: 307, // temporary redirect
//        host: 'http://example.com/her',
//    },
//    // ...
// }
// Note 1: No trailing slashes are required
// Note 2: Default redirect code is 301 (moved permanently)
exports.redirectHosts = {
}

// Add custom headers to all responses
//
// Example:
// exports.addHeaders = {
//     'server': 'whatever server',
//     'strict-transport-security': 'max-age=' + (60 * 60 * 24 * 30),
//     'cookie': ['a=b', 'c=d'],
// }
exports.addHeaders = [
]

// Serve static files from certain directories
//
// Example:
// exports.directories = {
//     '/public/': 'public/'
// }
exports.directories = {
}

// Remove headers from all proxy responses
//
// Example:
// exports.removeHeaders = [
//     'pragma',
//     'server',
//     'x-powered-by',
//     // ...
// ]
// Note: Headers should be all lowercase
exports.removeHeaders = [
]

// Custom error pages
//
// Example:
// exports.errorPages = {
//     403: 'error-pages/403-forbidden.html',
//     404: 'error-pages/404-not-found.html',
//     500: 'error-pages/500-internal-server-error.html',
//     502: 'error-pages/502-bad-gateway.html',
//     504: 'error-pages/504-gateway-timeout.html',
// }
exports.errorPages = {
}

// Replace error pages of proxied servers
//
// Example:
// exports.replaceErrorPages = {
//     403: 'replace-error-pages/404-forbidden.html',
//     404: 'replace-error-pages/404-not-found.html',
//     // ...
// }
exports.replaceErrorPages = {
}

// Listen to HTTPS instead of HTTP
//
// Example:
// exports.https = {
//     key: 'ssl/key',
//     cert: 'ssl/cert',
//     ca: [
//         'ssl/ca1',
//         'ssl/ca2',
//         ..
//     ],
// }

// Forbid access to certain user agents
//
// Example:
// exports.denyUserAgents = [
//     'SpammerBot',
//     'AnotherBot',
//     ...
// ]
exports.denyUserAgents = [
]
