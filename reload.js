#!/usr/bin/env node
process.chdir(__dirname)
const net = require('net')
const client = net.createConnection('server.sock', () => {
    client.end()
})
client.on('error', err => {
    const code = err.code
    if (code === 'ENOENT') console.error('ERROR: server not started')
    else if (code === 'EACCES') console.error('ERROR: permission denied')
    else throw err
})
