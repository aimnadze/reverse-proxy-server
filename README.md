Reverse Proxy Server
====================

A small, simple and fast reverse proxy built on Node.js to replace virtual
servers of Apache, Nginx, etc. Additional features:
* Add custom headers to all responses such as `server`, `strict-transport-security`, etc.
* Serve static files from certain directories.
* Remove headers such as `server`, `x-powered-by`, etc.
* Redirect hosts to other locations. (`www.example.com` to `example.com`, `my.example.com/anything` to `example.com/other-page/anything`, etc)
* Access logging.
* Rate limiting.

Scripts
-------

* `./restart.sh` - start/restart the server.
* `./stop.sh` - stop the server.
* `./clean.sh` - clean the server after an unexpected shutdown.
* `./rotate.sh` - clean old logs.

Configuration
-------------

`config.js` contains the configuration. See `example-config.js` for details.

See Also
--------

* [JP](https://gitlab.com/aimnadze/jp) for log file processing.
* [Forward Proxy Server](https://gitlab.com/aimnadze/forward-proxy-server)
